# Comunicação Laravel + SocketIO + NodeJS
<p align="center"><a href="https://blog.renatolucena.net/" target="_blank"><img src="laravel-node.png" width="500"></a></p>

<p align="center"><a href="https://gitlab.com/cpdrenato/laravel-nodejs-express-socketio" target="_blank"><img src="esquema.png" width="500"></a></p>


## Usado versão 7 do Laravel com php 7.4
`composer create-project --prefer-dist laravel/laravel Laravel-Socket-NodeJS "7.30.*"`

1. Execute o comando `yarn install` para instalar todas as bibliotecas e dependências.
2. Para instalar as dependências do Laravel, execute o comando `composer install`.
3. Depois de tudo instalado execute o comando `php artisan serve` para inicializar o servidor do Laravel.
4. Com outra instância de terminal execute o comando `yarn run runserver` para inicializar o servidor de Socket.

- 'http://localhost:8888'
- 'http://localhost:8000'

<p align="center"><a href="https://gitlab.com/cpdrenato/laravel-nodejs-express-socketio" target="_blank"><img src="2022-04-19_21-37.png" width="800"></a></p>
<p align="center"><a href="/" target="_blank"><img src="executando.gif" width="500"></a></p>

- Renato Lucena - 2022